package co.example.rabbit.util;

import co.example.rabbit.dto.*;

import java.util.ArrayList;
import java.util.List;

public class MessageFactory {

    public static MessageDTO createMessage(String pedido){
        return new MessageDTO(
                "tracking.guide",
                "ae959d66-c9e2-4aa0-9e5f-fd6890c4e7f2",
                new Data(createHeader(),createPayload(pedido))

        );
    }

    private static Header createHeader(){
        return new Header(
                "78354220-387a-454f-90f8-26c4a4c64d8c",
                "oms-tracking-generator",
                "oms-tracking-generator",
                "MAO",
                1643397060195L,
                "",
                createFail()
        );
    }

    private static List<Fail> createFail(){
        List<Fail> fails=new ArrayList<>();
        Fail fail=new Fail(
                "0",
                "Ejecución exitosa",
                ""
        );
        fails.add(fail);
        return fails;
    }

    private static Payload createPayload(String pedido){
        return new Payload(
                String.format("%s-01",pedido),
                "DIMARK DE COLOMBIA S.A.",
                "7245167",
                "Autopista Medellin km 7 parque celta bod 98-1 funza",
                "25286",
                "ricardo.jimenez@indusel.com.co",
                "Leonardo Motta Vargas",
                null,
                "2_1075237138",
                "calle 49c # 2-78 Bello",
                "+573115907822",
                "05088",
                "COORDINADORA_VMI_MEDELLIN",
                 null,
                "Bello",
                "leonardo.motta@sofka.com.co",
                "2022-01-28T19:03:26.661693",
                "Exito.com",
                 12798980,
                 null,
                new ArrayList<>()

        );
    }

   public static Plu createPlu(String plu){
        return new Plu(
                "1",
               "900074666",
               "ZAPF S.A.",
                plu,
               1,
               "000860534458BA01",
               "Nevera S Esch 895l Inoxi Fresh LG",
               "97.0",
               "178.0",
               "91.0",
                "9000.0",
               10699900,
               null,
               null,
               null
        );
   }
}
