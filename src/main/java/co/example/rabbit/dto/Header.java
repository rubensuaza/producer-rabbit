package co.example.rabbit.dto;

import java.util.List;

public class Header {

    private String transactionId;
    private String applicationId;
    private String hostname;
    private String user;
    private long transactionDate;
    private String esb;
    private List<Fail> errors;

    public Header(String transactionId, String applicationId, String hostname, String user, long transactionDate, String esb, List<Fail> errors) {
        this.transactionId = transactionId;
        this.applicationId = applicationId;
        this.hostname = hostname;
        this.user = user;
        this.transactionDate = transactionDate;
        this.esb = esb;
        this.errors = errors;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getHostname() {
        return hostname;
    }

    public String getUser() {
        return user;
    }

    public long getTransactionDate() {
        return transactionDate;
    }

    public String getEsb() {
        return esb;
    }

    public List<Fail> getErrors() {
        return errors;
    }
}
