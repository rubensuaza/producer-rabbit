package co.example.rabbit.dto;

public class Data {
    private Header header;
    private Payload data;

    public Data(Header header, Payload data) {
        this.header = header;
        this.data = data;
    }

    public Header getHeader() {
        return header;
    }

    public Payload getData() {
        return data;
    }
}
