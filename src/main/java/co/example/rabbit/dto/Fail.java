package co.example.rabbit.dto;

public class Fail {
    private String code;
    private String type;
    private String description;

    public Fail(String code, String type, String description) {
        this.code = code;
        this.type = type;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }
}
