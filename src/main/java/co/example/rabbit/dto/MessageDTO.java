package co.example.rabbit.dto;

public class MessageDTO {

    private String name;
    private String eventId;
    private Data data;

    public MessageDTO(String name, String eventId, Data data) {
        this.name = name;
        this.eventId = eventId;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public String getEventId() {
        return eventId;
    }

    public Data getData() {
        return data;
    }
}
