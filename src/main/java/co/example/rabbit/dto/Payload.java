package co.example.rabbit.dto;

import java.util.ArrayList;
import java.util.List;

public class Payload {

    private String pedido;
    private String nombreRemitente;
    private String telefonoRemitente;
    private String direccionRemitente;
    private String codigoDaneRemitente;
    private String emailRemitente;
    private String nombreDestinatario;
    private String nitDestinatario;
    private String cedulaDestinatario;
    private String direccionDestinatario;
    private String telefonoDestinatario;
    private String codigoDaneDestinatario;
    private String nombreTransportadora;
    private String isEnviosExito;
    private String nombreCiudad;
    private String emailDestinatario;
    private String fechaCompra;
    private String canal;
    private Integer totalCompra;
    private String centroDeCostos;
    private List<Plu> detallePorPlu=new ArrayList<>();

    public Payload(String pedido, String nombreRemitente, String telefonoRemitente, String direccionRemitente, String codigoDaneRemitente, String emailRemitente, String nombreDestinatario, String nitDestinatario, String cedulaDestinatario, String direccionDestinatario, String telefonoDestinatario, String codigoDaneDestinatario, String nombreTransportadora, String isEnviosExito, String nombreCiudad, String emailDestinatario, String fechaCompra, String canal, Integer totalCompra, String centroDeCostos, List<Plu> detallePorPlu) {
        this.pedido = pedido;
        this.nombreRemitente = nombreRemitente;
        this.telefonoRemitente = telefonoRemitente;
        this.direccionRemitente = direccionRemitente;
        this.codigoDaneRemitente = codigoDaneRemitente;
        this.emailRemitente = emailRemitente;
        this.nombreDestinatario = nombreDestinatario;
        this.nitDestinatario = nitDestinatario;
        this.cedulaDestinatario = cedulaDestinatario;
        this.direccionDestinatario = direccionDestinatario;
        this.telefonoDestinatario = telefonoDestinatario;
        this.codigoDaneDestinatario = codigoDaneDestinatario;
        this.nombreTransportadora = nombreTransportadora;
        this.isEnviosExito = isEnviosExito;
        this.nombreCiudad = nombreCiudad;
        this.emailDestinatario = emailDestinatario;
        this.fechaCompra = fechaCompra;
        this.canal = canal;
        this.totalCompra = totalCompra;
        this.centroDeCostos = centroDeCostos;
        this.detallePorPlu = detallePorPlu;
    }

    public void addPlu(Plu plu){detallePorPlu.add(plu);}

    public String getPedido() {
        return pedido;
    }

    public String getNombreRemitente() {
        return nombreRemitente;
    }

    public String getTelefonoRemitente() {
        return telefonoRemitente;
    }

    public String getDireccionRemitente() {
        return direccionRemitente;
    }

    public String getCodigoDaneRemitente() {
        return codigoDaneRemitente;
    }

    public String getEmailRemitente() {
        return emailRemitente;
    }

    public String getNombreDestinatario() {
        return nombreDestinatario;
    }

    public String getNitDestinatario() {
        return nitDestinatario;
    }

    public String getCedulaDestinatario() {
        return cedulaDestinatario;
    }

    public String getDireccionDestinatario() {
        return direccionDestinatario;
    }

    public String getTelefonoDestinatario() {
        return telefonoDestinatario;
    }

    public String getCodigoDaneDestinatario() {
        return codigoDaneDestinatario;
    }

    public String getNombreTransportadora() {
        return nombreTransportadora;
    }

    public String getIsEnviosExito() {
        return isEnviosExito;
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public String getEmailDestinatario() {
        return emailDestinatario;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public String getCanal() {
        return canal;
    }

    public Integer getTotalCompra() {
        return totalCompra;
    }

    public String getCentroDeCostos() {
        return centroDeCostos;
    }

    public List<Plu> getDetallePorPlu() {
        return detallePorPlu;
    }
}
