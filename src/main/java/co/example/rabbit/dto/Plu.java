package co.example.rabbit.dto;

public class Plu {
    private String tipoNegociacion;
    private String nitProveedor;
    private String nombreProveedor;
    private String plu;
    private int cantidadDespachada;
    private String codigoDependenciaDespacha;
    private String descripcionPlu;
    private String largo;
    private String alto;
    private String ancho;
    private String peso;
    private Integer valorPlu;
    private String idProductPicking;
    private String idCotizacion;
    private String price;

    public Plu(String tipoNegociacion, String nitProveedor, String nombreProveedor, String plu, int cantidadDespachada, String codigoDependenciaDespacha, String descripcionPlu, String largo, String alto, String ancho, String peso, Integer valorPlu, String idProductPicking, String idCotizacion, String price) {
        this.tipoNegociacion = tipoNegociacion;
        this.nitProveedor = nitProveedor;
        this.nombreProveedor = nombreProveedor;
        this.plu = plu;
        this.cantidadDespachada = cantidadDespachada;
        this.codigoDependenciaDespacha = codigoDependenciaDespacha;
        this.descripcionPlu = descripcionPlu;
        this.largo = largo;
        this.alto = alto;
        this.ancho = ancho;
        this.peso = peso;
        this.valorPlu = valorPlu;
        this.idProductPicking = idProductPicking;
        this.idCotizacion = idCotizacion;
        this.price = price;
    }

    public String getTipoNegociacion() {
        return tipoNegociacion;
    }

    public String getNitProveedor() {
        return nitProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public String getPlu() {
        return plu;
    }

    public int getCantidadDespachada() {
        return cantidadDespachada;
    }

    public String getCodigoDependenciaDespacha() {
        return codigoDependenciaDespacha;
    }

    public String getDescripcionPlu() {
        return descripcionPlu;
    }

    public String getLargo() {
        return largo;
    }

    public String getAlto() {
        return alto;
    }

    public String getAncho() {
        return ancho;
    }

    public String getPeso() {
        return peso;
    }

    public Integer getValorPlu() {
        return valorPlu;
    }

    public String getIdProductPicking() {
        return idProductPicking;
    }

    public String getIdCotizacion() {
        return idCotizacion;
    }

    public String getPrice() {
        return price;
    }
}
