package co.example.rabbit;

import co.example.rabbit.dto.MessageDTO;
import co.example.rabbit.dto.Plu;
import co.example.rabbit.util.MessageFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.Map;

@Controller
public class RabbitProducer {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Scheduled(cron="0 09 12 * * *",zone = "America/Bogota")
    public void sendDemoQueue() {
        for (int i=0;i<5;i++){
            MessageDTO message=MessageFactory.createMessage(String.valueOf(i));
            int numItems= ((int) (Math.random()*4))+1;
            for (int j=0;j<numItems;j++){
                Plu plu=MessageFactory.createPlu(String.valueOf(j));
                message.getData().getData().addPlu(plu);
            }
            //String message=createMessage(String.valueOf(i));
            rabbitTemplate.convertAndSend("OMS.TransportIntegrator.queue",message);
            System.out.println("mensaje "+i+" enviado");
        }
    }

    private static Map<String,Object> createObject(String message){
        Map<String,Object> object=new HashMap<>();
        object.put("id",message);
        object.put("nombre","nombre_"+message);
        object.put("apellido","apellido_"+message);
        object.put("cedula","123"+message);
        object.put("edad",38);
        object.put("sexo","masculino");

        return object;
    }
    private static String createMessage(String id){
        return String.format("{\"name\": \"tracking.guide\",\"eventId\": \"ae959d66-c9e2-4aa0-9e5f-fd6890c4e7f2\",\"data\": {\"header\": {\"transactionId\": \"78354220-387a-454f-90f8-26c4a4c64d8c\",\"applicationId\": \"oms-tracking-generator\",\"hostname\": \"oms-tracking-generator\",\"user\": \"MAO\",\"transactionDate\": 1643397060195,\"esb\": null,\"errors\": [{\"code\": \"0\",\"type\": \"Ejecución exitosa\",\"description\": \"\"}]},\"data\": {\"pedido\": \"%s\",\"nombreRemitente\": \"DIMARK DE COLOMBIA S.A.\",\"telefonoRemitente\": \"7245167\",\"direccionRemitente\": \"Autopista Medellin km 7 parque celta bod 98-1 funza\",\"codigoDaneRemitente\": \"25286\",\"emailRemitente\": \"ricardo.jimenez@indusel.com.co\",\"nombreDestinatario\": \"Leonardo Motta Vargas\",\"nitDestinatario\": null,\"cedulaDestinatario\":\"2_1075237138\",\"direccionDestinatario\": \"calle 49c # 2-78 Bello\",\"telefonoDestinatario\": \"+573115907822\",\"codigoDaneDestinatario\": \"05088\",\"nombreTransportadora\": \"COORDINADORA_VMI_MEDELLIN\",\"isEnviosExito\": null,\"nombreCiudad\": \"Bello\",\"emailDestinatario\": \"leonardo.motta@sofka.com.co\",\"fechaCompra\": \"2022-01-28T19:03:26.661693\",\"canal\": \"Exito.com\",\"totalCompra\": 12798980,\"centroDeCostos\": null,\"detallePorPlu\": [{\"tipoNegociacion\": \"1\",\"nitProveedor\": \"860534458\",\"nombreProveedor\": \"DIMARK DE COLOMBIA S.A.\",\"plu\": \"42445\",\"cantidadDespachada\": 1.0,\"codigoDependenciaDespacha\": \"000860534458BA01\",\"descripcionPlu\": \"Nevera S Esch 895l Inoxi Fresh LG\",\"largo\": \"97.0\",\"alto\": \"178.0\",\"ancho\": \"91.0\",\"peso\": \"9000.0\",\"valorPlu\": 10699900,\"idProductPicking\": null,\"idCotizacion\": null,\"price\": null}, {\"tipoNegociacion\": \"1\",\"nitProveedor\": \"860534458\",\"nombreProveedor\": \"DIMARK DE COLOMBIA S.A.\",\"plu\": \"42427\",\"cantidadDespachada\": 1.0,\"codigoDependenciaDespacha\": \"000860534458BA01\",\"descripcionPlu\": \"Nevera Sin Escarcha 394 L Gris LG\",\"largo\": \"71.0\",\"alto\": \"177.0\",\"ancho\": \"68.0\",\"peso\": \"87000.0\",\"valorPlu\": 2049900,\"idProductPicking\": null,\"idCotizacion\": null,\"price\": null},{\"tipoNegociacion\": \"1\",\"nitProveedor\": \"860534459\",\"nombreProveedor\": \"OTRO\",\"plu\": \"42445\",\"cantidadDespachada\": 1.0,\"codigoDependenciaDespacha\": \"000860534458BA01\",\"descripcionPlu\": \"Nevera S Esch 895l Inoxi Fresh LG\",\"largo\": \"97.0\",\"alto\": \"178.0\",\"ancho\": \"91.0\",\"peso\": \"9000.0\",\"valorPlu\": 10699900,\"idProductPicking\": null,\"idCotizacion\": null,\"price\": null}]}}}",id);
    }


}
